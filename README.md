# PigKnows Code Test

## The Task

Create a small Javascript application that pulls datafrom an api (https://randomuser.me/) anddisplay a summary of 3 or more users at a time. Youshould have the ability to select a user andhave it show more detail about that user either ina modal or a new page. In addition theapplication should also have an option to show a newset of users without reloading the page(generate / refresh button or similar).

## Technologies that must be used

1. Javascript
2. React

As a note you do not have to be a master of any ofthese technologies to create this application.A key part of this position will be the ability tolearn and test new technologies and to keep highquality code and best practices throughout. Feel freeto use any other packages / modules thatmay assist you. If you are worried about design feelfree to use any components or libraries toassist with that. We at PigKnows use https://material-ui-next.com/so feel free to use it oranything else to help you accomplish this task

## Bonus

Make the application feel more complete with placeholdertext, images and navigation. Feel freeto add as much information and detail as possible
