import './App.css'
import React from 'react'
import { Classy } from './components/Pages/Classy'
import Hooks from './components/Pages/Hooks'
import Navbar from './components/Layout/Navbar'
import About from './components/Pages/About'
import Home from './components/Pages/Home'
import {
	BrowserRouter as Router,
	Switch,
	Route
} from 'react-router-dom'

function App() {
	return (
		<Router>
			<div style={{ alignContent: 'center' }}>
				<Navbar />
				<div style={{ maxWidth: 1000, margin: 'auto' }} className='container'>
					<Switch>
						<Route exact path='/classy'>
							<Classy />
						</Route>
						<Route exact path='/hooks'>
							<Hooks />
						</Route>
						<Route exact path='/about'>
							<About />
						</Route>
						<Route path='/'>
							<Home />
						</Route>
					</Switch>
				</div>
			</div>
		</Router>
	)
}

export default App
