import React, { useEffect, useState } from 'react'
import { PeopleListItemHook } from '../People/PeopleListItemHook'
import { PersonDetailHook } from '../People/PersonDetailHook'
import { LoadingBar } from '../Layout/LoadingBar'
import { Modal, Button } from 'material-ui-next'
import axios from 'axios'

const Hooks = () => {
	const [loading, setLoading] = useState(false)
	const [people, setPeople] = useState([])
	const [modalOpen, setModalOpen] = useState(false)
	const [selectedPerson, setSelectedPerson] = useState(null)

	const getPeople = async () => {
		setLoading(true)
		const res = await axios.get(
			'https://randomuser.me/api/?results=50'
		)
		console.log(res.data.results)
		setLoading(false)
		setPeople(res.data.results)
	}

	const clearPeople = () => setPeople([])

	const handleCardClick = (selection) => {
		setModalOpen(true)
		setSelectedPerson(selection)
	}

	const handleCloseModal = () => {
		setModalOpen(false)
		setSelectedPerson(null)
	}

	useEffect(() => {
		getPeople()
	}, [])

	return (
		<div className='container'>
			<p>This page uses hooks</p>
			{loading ? (
				<LoadingBar></LoadingBar>
			) : (
				<React.Fragment>
					<div style={{ display: 'block', marginBottom: '10px' }}>
						{people.length === 0 && (<Button variant='raised' color='primary' onClick={getPeople}>
                                Get people!
						</Button>
						)}
						{people.length > 0 && (<Button variant='raised' color='secondary' onClick={clearPeople}>
                                Clear people!
						</Button>
						)}
					</div>
					<div className='people-list'>
						{people.map((person) => (
							<PeopleListItemHook key={person.cell} person={person} onCardClick={handleCardClick} />
						))}
					</div>
				</React.Fragment>
			)}
			<Modal
				open={modalOpen}
				onClose={handleCloseModal}
				aria-labelledby='simple-modal-title'
				aria-describedby='simple-modal-description'
			>
				<PersonDetailHook person={selectedPerson} />
			</Modal>
		</div>
	)
}

export default Hooks