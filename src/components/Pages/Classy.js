import React, { Component } from 'react'
import { PeopleListItem } from '../People/PeopleListItem'
import { PersonDetail } from '../People/PersonDetail'
import { LoadingBar } from '../Layout/LoadingBar'
import { Modal, Button } from 'material-ui-next'
import axios from 'axios'


export class Classy extends Component {
    state = {
        loading: false,
        modalOpen: false,
        selectedPerson: null,
        people: []
    }

    componentDidMount = async() => this.getPeople()

    getPeople = async () => {
        this.setState({ loading: true })
        const res = await axios.get(
            `https://randomuser.me/api/?results=50`
        )
        console.log(res.data.results)
        this.setState({ people: res.data.results, loading: false })
    }

    clearPeople = () => this.setState({ people: [] })

    handleCardClick = (selection) => this.setState({ modalOpen: true, selectedPerson: selection })

    handleCloseModal = () => this.setState({ modalOpen: false, selectedPerson: null })

    render() {
        const { people, loading } = this.state

        return (
            <div className='container'>
                <p>This page uses class components</p>
                {loading ? (
                    <LoadingBar></LoadingBar>
                ) : (
                    <React.Fragment>
                        <div style={{ display: 'block', marginBottom: '10px' }}>
                            {people.length === 0 && (<Button variant='raised' color='primary' onClick={this.getPeople}>
                                Get people!
                            </Button>
                            )}
                            {people.length > 0 && (<Button variant='raised' color='secondary' onClick={this.clearPeople}>
                                Clear people!
                            </Button>
                            )}
                        </div>
                        <div className='people-list'>
                            {people.map((person) => (
                                <PeopleListItem key={person.cell} person={person} onCardClick={this.handleCardClick} />
                            ))}
                        </div>
                    </React.Fragment>
                )}
                <Modal
                    open={this.state.modalOpen}
                    onClose={this.handleCloseModal}
                    aria-labelledby='simple-modal-title'
                    aria-describedby='simple-modal-description'
                >
                    <PersonDetail person={this.state.selectedPerson} />
                </Modal>
            </div>
        )

    }
}