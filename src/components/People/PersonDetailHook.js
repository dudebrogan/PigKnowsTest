import React from 'react'

export const PersonDetailHook = ({ person: { picture,name,phone,location,email,login } }) => {
	return (
		<div className='person-detail-container'>
			{picture && (
				<img src={picture.large} className='detail-image' alt={name.first} />
			)}
			<ul>
				<li>
					{name && (
						<React.Fragment>
							<strong>Name: </strong> {`${name.first} ${name.last}`}
						</React.Fragment>
					)}
				</li>
				<li>
					{phone && (
						<React.Fragment>
							<strong>Phone: </strong> {`${phone}`}
						</React.Fragment>
					)}
				</li>
				<li>
					{email && (
						<React.Fragment>
							<strong>Email: </strong> {`${email}`}
						</React.Fragment>
					)}
				</li>
				<li>
					{location && (
						<React.Fragment>
							<strong>Location: </strong> {`${location.city}, ${location.country}`}
						</React.Fragment>
					)}
				</li>
				<li>
					{login && (
						<React.Fragment>
							<strong>Username: </strong> {`${login.username}`}
						</React.Fragment>
					)}
				</li>
			</ul>
		</div>
	)
}

export default PersonDetailHook