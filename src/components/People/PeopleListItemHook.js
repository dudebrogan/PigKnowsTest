import React from 'react'
import { Card } from 'material-ui-next'
import { CardContent } from 'material-ui-next'

export const PeopleListItemHook = (props) => {
	const { picture,name,phone } = props.person

	const handleClick = () => {
		props.onCardClick(props.person)
	}

	return (
		<Card className='card-container' onClick={handleClick}>
			<img src={picture.large} className='card-image' alt={name.first} />
			<CardContent style={{ backgroundColor: 'transparent' }}>
				<ul>
					<li>
						{name && (
							<React.Fragment>
								<strong>Name: </strong> {`${name.first} ${name.last}`}
							</React.Fragment>
						)}
					</li>
					<li>
						{phone && (
							<React.Fragment>
								<strong>Phone: </strong> {`${phone}`}
							</React.Fragment>
						)}
					</li>
				</ul>
			</CardContent>
		</Card>
	)
}

export default PeopleListItemHook