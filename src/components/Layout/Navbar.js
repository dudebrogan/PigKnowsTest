import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
	return (
		<nav className='navbar'>
			<ul>
				<li>
					<Link to='/classy'>Classy</Link>
				</li>
				<li>
					<Link to='/hooks'>Hooks</Link>
				</li>
				<li>
					<Link to='/about'>About</Link>
				</li>
			</ul>
		</nav>
	)
}

export default Navbar