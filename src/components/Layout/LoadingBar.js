import React, { Fragment } from 'react'
import loadingBar from './LoadingBar.gif'

export const LoadingBar = () => (
	<Fragment>
		<img
			src={loadingBar}
			alt='Loading...'
			style={{ height: '50px', width: 'auto', margin: 'auto', display: 'block' }}
		/>
	</Fragment>
)

export default LoadingBar